<?php

namespace RR\EventBusLaravel;

use Exception;
use Illuminate\Support\ServiceProvider;

/**
 * Class BusServiceProvider
 * @package App\Base\Bus
 */
class BusServiceProvider extends ServiceProvider
{
    /**
     * @throws Exception
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/event_bus.php' => config_path('event_bus.php'),
            __DIR__ . '/routes/event_bus.php' => base_path('routes/event_bus.php'),
        ], 'event_bus');

        $this->app->singleton(EventBusRouter::class);
        $this->app->singleton(EventBusFactory::class);

        if ($this->app->runningInConsole()) {
            if(!file_exists(config_path('event_bus.php')) || !file_exists(base_path('routes/event_bus.php'))) {
                logger()
                    ->channel('stderr')
                    ->alert('event_bus assets are not published. Please run: php artisan vendor:publish --tag=event_bus');
            } else {
                $this->checkConfig(config('event_bus'));
            }

            /** Add consumer command */
            $this->commands([
                BusDaemon::class,
            ]);
        }
    }

    /**
     * @param $config
     * @throws Exception
     */
    private function checkConfig($config)
    {
        if (!$config) {
            throw new Exception('event_bus config empty');
        }

        if (!isset($config['connections']) || !is_array($config['connections'])) {
            throw new Exception('event_bus config "connections" empty');
        }

        if (!isset($config['default']) || !is_string($config['default'])) {
            throw new Exception('event_bus config "default" empty');
        }

        if (!isset($config['connections'][$config['default']])) {
            throw new Exception($config['default'] . ' event bus default connection is not exists');
        }

        foreach ($config['connections'] as $name => $connection) {
            if (!isset($connection['driver'])) {
                throw new Exception($name . ' event bus connection has no driver');
            }
        }
    }
}
