<?php

namespace RR\EventBusLaravel;

use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Support\Facades\Facade;
use RR\EventBus\ConsumerInterface;
use RR\EventBus\ProducerInterface;
use RR\EventBus\Transports;

/**
 * Class EventBusFactory
 * @package RR\EventBusLaravel
 */
class EventBusFactory extends Facade
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * Available drivers
     *
     * @var array
     */
    protected $drivers;

    /**
     * @var mixed
     */
    protected $connection;

    /**
     * EventBusFactory constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->connection = $config->get('event_bus.default');
        $this->drivers = Transports::map();
    }

    /**
     * @param string $connection
     * @return $this
     */
    public function connection(string $connection)
    {
        $this->connection = $connection;

        return $this;
    }

    /**
     * @return ProducerInterface
     */
    public function producer()
    {
        $connectionConfig = $this->config->get('event_bus.connections.' . $this->connection);
        $class = $this->drivers[$connectionConfig['driver']]['producer'];

        return new $class($connectionConfig);
    }

    /**
     * @return ConsumerInterface
     */
    public function consumer()
    {
        $connectionConfig = $this->config->get('event_bus.connections.' . $this->connection);
        $class = $this->drivers[$connectionConfig['driver']]['consumer'];

        return new $class($connectionConfig);
    }
}