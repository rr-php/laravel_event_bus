<?php

namespace RR\EventBusLaravel;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Contracts\Config\Repository as Config;

/**
 * Class BusDaemon
 * @package RR\EventBusLaravel
 */
class BusDaemon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event_bus:listen {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Message broker daemon';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var EventBusFactory
     */
    private $eventBusFactory;

    /**
     * @var EventBusRouter
     */
    private $eventBusRouter;

    /**
     * BusDaemon constructor.
     *
     * @param Config $config
     * @param EventBusFactory $eventBusFactory
     * @param EventBusRouter $eventBusRouter
     */
    public function __construct(Config $config, EventBusFactory $eventBusFactory, EventBusRouter $eventBusRouter)
    {
        $this->config = $config;
        $this->eventBusFactory = $eventBusFactory;
        $this->eventBusRouter = $eventBusRouter;

        parent::__construct();
    }

    /**
     * @throws Exception
     */
    public function handle()
    {
        $connection = $this->argument('name');

        if (!$connection) {
            $connection = $this->config->get('event_bus.default');
        }

        if (file_exists(base_path('routes/event_bus.php'))) {
            require base_path('routes/event_bus.php');
        }

        $this->eventBusFactory->connection($connection)->consumer();

        if ($this->eventBusRouter->has($connection)) {
            $this->eventBusFactory
                ->connection($connection)
                ->consumer()
                ->consume($this->eventBusRouter->get($connection));
        } else {
            throw new Exception('There are no routes for ' . $connection . ' event bus connection');
        }
    }
}
