<?php

namespace RR\EventBusLaravel;

use Illuminate\Support\Facades\Facade;

class EventBus extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return EventBusFactory::class; }
}
