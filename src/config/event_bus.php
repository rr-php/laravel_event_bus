<?php
return [
    'default' => 'my-kafka',

    'connections' => [
        'my-kafka' => [
            'driver' => 'kafka',
            // https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
            'rdkafka_conf' => [
                'metadata.broker.list' => env('KAFKA_BROKERS'),
                'enable.auto.commit' => 'false',
                'group.id' => 'api',
                'auto.offset.reset' => 'smallest',
                'offset.store.method' => 'broker',
                'queued.max.messages.kbytes' => '100000',
                'topic.metadata.refresh.sparse' => 'true',
                'topic.metadata.refresh.interval.ms' => '600000',
                'auto.commit.enable' => 'false',
            ],
            'topic_conf' => [
                'message.timeout.ms' => 1000*20,
            ],
            'timeout' => 1000*120,
            'prefix' => env('KAFKA_PREFIX', ''),
        ],
    ],
];