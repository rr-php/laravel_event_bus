<?php

namespace RR\EventBusLaravel;

use Exception;
use RR\EventBus\BusRouter;

/**
 * Class EventBusRouter
 * @package RR\EventBusLaravel
 */
class EventBusRouter
{
    /**
     * @var array
     */
    public $connections = [];

    /**
     * @param $name
     * @param $callback
     * @throws Exception
     */
    public function connection(string $name, \Closure $callback)
    {
        $router = app(BusRouter::class);
        $callback($router);
        $this->connections[$name] = $router;
    }

    /**
     * @param $name
     * @return bool
     */
    public function has($name)
    {
        return array_key_exists($name, $this->connections);
    }


    /**
     * @param $name
     * @return mixed
     */
    public function get($name)
    {
        return $this->connections[$name];
    }
}
