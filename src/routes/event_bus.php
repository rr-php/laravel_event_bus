<?php

use RR\EventBusLaravel\EventBusRouter;
use RR\EventBus\BusRouter;

/** @var EventBusRouter $client */
$client = resolve(EventBusRouter::class);

$client->connection('my-kafka', function (BusRouter $router) {
    $router->add('test_topic', 'App/Some/Path/Handler');
});


