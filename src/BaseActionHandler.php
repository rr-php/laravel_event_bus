<?php

namespace RR\EventBusLaravel;

use Exception;
use RR\EventBus\Message;
use RR\EventBus\MessageProcessorInterface;

/**
 * Class BaseActionHandler
 * @package RR\EventBusLaravel
 */
class BaseActionHandler implements MessageProcessorInterface
{
    /**
     * @param Message $message
     * @return mixed
     * @throws Exception
     */
    public function process(Message $message): bool
    {
        if (isset($message['action']) && isset($message['data'])) {
            $action = $message['action'];
            $data = $message['data'];
        } else {
            $action = '__invoke';
            $data = $message;
        }

        if (!method_exists($this, $action)) {
            throw new Exception(__CLASS__ . ':' . $action . ' is not exists');
        }

        return (bool)$this->{$action}($data);
    }
}
