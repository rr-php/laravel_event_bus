## Install

```bash

Add to composer.json

    "repositories": [
        {
            "type": "gitlab",
            "url": "https://gitlab.com/rr-php/laravel_event_bus.git"
        },
        {
            "type": "gitlab",
            "url": "https://gitlab.com/rr-php/event_bus.git"
        }
    ],

composer require rr/laravel_event_bus

php artisan vendor:publish --tag=event_bus

```

## Example consumer

```bash
php artisan bus:listen my-kafka
```
## Example producer

```php
//injection
__construct(EventBusFactory $eventBusFactory) {
    $this->kafka = $eventBusFactory
        ->connection('my-kafka') // optional
        ->producer();
}

//factory
$producer = app(EventBusFactory::class)
    ->connection('my-kafka') // optional
    ->producer();
    
//facade
$producer = EventBus::connection('my-kafka') // optional
    ->producer();
    
    
$producer->produce('topic', 'message', 'some_key_to_enable_correct_message_order')

```



